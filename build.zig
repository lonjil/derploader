const std = @import("std");
const deps = @import("./deps.zig");

fn blah(val: anytype, val2: anytype) bool {
    return val != null and val.? == val2;
}

pub fn build(b: *std.build.Builder) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    var target = b.standardTargetOptions(
        .{
            //.default_target = .{
            //    .abi = .musl,
            //},
        },
    );
    if (target.isGnuLibC()) target.setGnuLibCVersion(2, 28, 0);

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();
    const strip = b.option(bool, "strip", "strip executable");
    const static = b.option(bool, "static", "compile a static executable");

    const exe = b.addExecutable("derploader", "src/main.zig");
    if (strip != null) {
        exe.strip = strip.?;
    }
    if (static != null and static.? == true) {
        exe.linkage = .static;
    }
    deps.addAllTo(exe);
    //exe.linkLibC();
    exe.single_threaded = true;
    if (blah(static, true)) {
        exe.addIncludeDir("/usr/include/");
        exe.addObjectFile("/usr/lib/libcurl.a");
        exe.addObjectFile("/usr/lib/libssl.a");
        exe.addObjectFile("/usr/lib/libcrypto.a");
        exe.addObjectFile("/usr/lib/libnghttp2.a");
        exe.addObjectFile("/usr/lib/libbrotlicommon.a");
        exe.addObjectFile("/usr/lib/libbrotlidec.a");
        exe.addObjectFile("/usr/lib/libbrotlienc.a");
        exe.addObjectFile("/lib/libz.a");
    } else {
        exe.linkSystemLibrary("libcurl");
    }
    exe.setTarget(target);
    exe.setBuildMode(mode);
    exe.install();

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
