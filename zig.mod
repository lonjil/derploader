id: 1m1xcon88qpj9vi17d8yo2743g0pqswk91ep21b0v139bu5s
name: derploader
main: src/main.zig
dev_dependencies:
  - src: git https://github.com/Hejsil/zig-clap branch-zig-master
  - src: git https://github.com/vrischmann/zig-sqlite
    c_source_flags:
      - -DSQLITE_ENABLE_JSON1
      - -DSQLITE_ENABLE_DQS=0
      - -DSQLITE_THREADSAFE=0
      - -DSQLITE_DEFAULT_MEMSTATUS=0
      - -DSQLITE_DEFAULT_WAL_SYNCHRONOUS=1
      - -DSQLITE_LIKE_DOESNT_MATCH_BLOBS
      - -DSQLITE_MAX_EXPR_DEPTH=0
      - -DSQLITE_OMIT_DEPRECATED
      - -DSQLITE_OMIT_SHARED_CACHE
      - -DSQLITE_DEFAULT_FOREIGN_KEYS
      - -DHAVE_FDATASYNC
      - -DHAVE_ISNAN
      - -DSQLITE_USE_URI
      - -DSQLITE_ALLOW_URI_AUTHORITY
      - -DSQLITE_ENABLE_BYTECODE_VTAB
      - -DSQLITE_ENABLE_COLUMN_METADATA
      - -DSQLITE_ENABLE_DBPAGE_VTAB
      - -DSQLITE_ENABLE_DBSTAT_VTAB
      - -DSQLITE_ENABLE_EXPLAIN_COMMENTS
      - -DSQLITE_ENABLE_FTS5
      - -DSQLITE_ENABLE_MATH_FUNCTIONS
      - -DSQLITE_ENABLE_BATCH_ATOMIC_WRITE=1
    c_source_files:
      - ../../../../../../../../sqlite/sqlite3.c
  - src: git https://github.com/nektro/zig-json
  - src: git https://github.com/MasterQ32/zig-uri/
